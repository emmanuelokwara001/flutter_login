import 'package:service_locator/service_locator.dart';
import 'package:user_repository/src/models/user.dart';

class UserRepository {
  // secure storage
  SecureStorage _ss = sl<SecureStorage>();
  List<User> accounts = [
    User(
      id: '4HIDJ6fk23K11',
      username: 'flutiour',
      name: 'flutter',
      surname: 'juniour',
      email: 'flutterjr@gmail.com',
      phoneNumber: '+27849334755',
      password: '0fduhfa9237D',
    ),
    User(
      id: '4HIDJ6fk23K98',
      username: 'jamief',
      name: 'Jamielee',
      surname: 'jo',
      email: 'jo@gmail.com',
      phoneNumber: '+27934023832',
      password: 'P@\$\$123w0rD#01',
    ),
    User(
      id: '4HIDJ6fk23K33',
      username: 'michel',
      name: 'michael',
      surname: 'el',
      email: 'elmc@gmail.com',
      phoneNumber: '+27084023037',
      password: 'Mc@#28390ld',
    ),
  ];

  // user
  User? _user;

  Future<User?> getUser() async {
    String? username = await _ss.readKey('username');
    if (_user != null) return _user;
    User user = accounts.singleWhere((element) => element.username == username);
    return user;
  }

  Future<bool> CheckUser(String username) async {
    return accounts.any((user) => user.username == username);
  }

  Future<void> addUser(User user) async {
    accounts.add(user);
  }
}
