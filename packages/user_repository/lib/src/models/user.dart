import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String id;
  final String? name;
  final String? username;
  final String? surname;
  final String? email;
  final String? phoneNumber;
  final String? password;

  const User({
    required this.id,
    this.username,
    this.name,
    this.surname,
    this.email,
    this.phoneNumber,
    this.password,
  });

  static const empty = User(id: '_');

  @override
  List<Object?> get props => [name, surname, email, phoneNumber, password];
}
