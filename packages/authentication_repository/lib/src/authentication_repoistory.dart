import 'dart:async';
import 'package:service_locator/service_locator.dart';

// Authentication Statuses
enum AuthenticationStatus { unknown, authenticated, unauthenticated }

// Authentication repository
class AuthenticationRepository {
  final SecureStorage fss = sl<SecureStorage>();
  // Stream controller
  final _controller = StreamController<AuthenticationStatus>();
  Stream<AuthenticationStatus> get status async* {
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<void> logIn({
    required String username,
    required String password,
  }) async {
    await Timer(
      Duration(seconds: 2),
      () {
        _controller.add(AuthenticationStatus.authenticated);
        fss.writeValue('username', username);
        fss.writeValue('password', password);
      },
    );
  }

  FutureOr<void> logOut() async {
    await fss.deleteValue('username');
    await fss.deleteValue('password');
    _controller.add(AuthenticationStatus.unauthenticated);
  }
}
