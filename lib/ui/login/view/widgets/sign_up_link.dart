import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SignUpLink extends StatelessWidget {
  const SignUpLink({super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        context.go('/signup');
      },
      child: const Text(
        'SignUp',
      ),
    );
  }
}
