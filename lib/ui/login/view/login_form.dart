import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:login_registration_app/ui/login/view/widgets/sign_up_link.dart';
import '../bloc/login_bloc.dart';
import 'widgets/login_button.dart';
import 'widgets/password_input.dart';
import 'widgets/username_input.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                elevation: 3.0,
                backgroundColor: Colors.grey.shade100,
                content: const Text(
                  'Authentication Failure',
                  style: TextStyle(
                    color: Color.fromARGB(255, 215, 55, 55),
                  ),
                ),
              ),
            );
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: const [
              Text(
                'Login',
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.all(20),
          ),
          const UsernameInput(),
          const Padding(padding: EdgeInsets.all(12)),
          const PasswordInput(),
          const Padding(padding: EdgeInsets.all(12)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              LoginButton(),
              Padding(
                padding: EdgeInsets.all(20),
              ),
              SignUpLink(),
            ],
          ),
        ],
      ),
    );
  }
}
