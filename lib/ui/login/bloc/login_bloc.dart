import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:user_repository/user_repository.dart';

import '../models/password.dart';
import '../models/username.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    required AuthenticationRepository authRepo,
    required UserRepository userRepo,
  })  : _authRepo = authRepo,
        _userRepository = userRepo,
        super(const LoginState()) {
    on<LoginUsernameChanged>(_onUsernameChanged);
    on<LoginPasswordChanged>(_onPasswordChanged);
    on<LoginSubmitted>(_onSubmitted);
  }

  final AuthenticationRepository _authRepo;
  final UserRepository _userRepository;

  void _onUsernameChanged(
    LoginUsernameChanged event,
    Emitter<LoginState> emit,
  ) {
    final username = Username.dirty(event.username);
    emit(
      state.copyWith(
        username: username,
        status: Formz.validate(
          [state.password, username],
        ),
      ),
    );
  }

  void _onPasswordChanged(
    LoginPasswordChanged event,
    Emitter<LoginState> emit,
  ) {
    final password = Password.dirty(event.password);
    emit(
      state.copyWith(
        password: password,
        status: Formz.validate(
          [password, state.username],
        ),
      ),
    );
  }

  Future<void> _onSubmitted(
      LoginSubmitted event, Emitter<LoginState> emit) async {
    emit(
      state.copyWith(
        status: FormzStatus.submissionInProgress,
      ),
    );
    if (state.status.isValidated) {
      try {
        bool isUser = await _userRepository.CheckUser(state.username.value);

        if (isUser) {
          await _authRepo.logIn(
            username: state.username.value,
            password: state.password.value,
          );
          return emit(
            state.copyWith(
              status: FormzStatus.submissionSuccess,
            ),
          );
        }
        return emit(
          state.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        );
      } catch (_) {
        emit(state.copyWith(status: FormzStatus.submissionFailure));
      }
    }
  }
}
