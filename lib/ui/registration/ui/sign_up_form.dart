import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'widgets/create_account_button.dart';
import 'widgets/email_field.dart';
import 'widgets/name_field.dart';
import 'widgets/password_field.dart';
import 'widgets/phone_number_field.dart';
import 'widgets/surname_field.dart';
import 'widgets/username_field.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(
              40,
            ),
          ),
          Row(
            children: const <Widget>[
              Text(
                'Sign Up',
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const NameField(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const Surname(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const UsernameField(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const EmailField(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const PhoneNumberField(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const PasswordField(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const CreateAccountButton(),
          const Padding(
            padding: EdgeInsets.all(
              10,
            ),
          ),
          const Text(
            'Already have an account ?',
            style: TextStyle(color: Colors.grey),
          ),
          const Padding(padding: EdgeInsets.all(5)),
          GestureDetector(
            onTap: () {
              context.go('/login');
            },
            child: const Text('Login'),
          )
        ],
      ),
    );
  }
}
