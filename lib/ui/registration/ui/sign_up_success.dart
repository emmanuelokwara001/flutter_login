import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SignUpSuccessPage extends StatelessWidget {
  const SignUpSuccessPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.onBackground,
        leading: IconButton(
          onPressed: () {
            context.go('/login');
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.check_circle,
                size: 100,
                color: Colors.green,
              ),
              Padding(
                padding: EdgeInsets.all(14.0),
                child: Text(
                  'Your Account has been created!',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
