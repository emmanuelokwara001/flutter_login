import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:go_router/go_router.dart';

import '../../bloc/sign_up_bloc.dart';

class CreateAccountButton extends StatelessWidget {
  const CreateAccountButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpBloc, SignUpState>(
      listener: (context, state) {
        if (state.status.isSubmissionSuccess) {
          context.go('/signup/success');
        } else if (state.status.isSubmissionFailure) {
          context.go('/signup/failure');
        }
      },
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          side: BorderSide(
            width: 1,
            color: Theme.of(context).primaryColor,
          ),
        ),
        onPressed: () {
          context.read<SignUpBloc>().add(SignUpSubmitUserEvent());
        },
        child: const Text(
          'CREATE ACCOUNT',
          style: TextStyle(fontSize: 15),
        ),
      ),
    );
  }
}
