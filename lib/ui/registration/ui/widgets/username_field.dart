import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/sign_up_bloc.dart';

class UsernameField extends StatelessWidget {
  const UsernameField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpBloc, SignUpState>(
      builder: (context, state) {
        return TextField(
          autofocus: false,
          onChanged: (value) {
            context.read<SignUpBloc>().add(SignUpUsernameEvent(value));
          },
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: const OutlineInputBorder(),
            labelText: 'Username',
            errorText: state.username.invalid ? "Invalid username" : null,
          ),
        );
      },
    );
  }
}
