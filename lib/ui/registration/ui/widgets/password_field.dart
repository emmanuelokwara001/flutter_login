import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/sign_up_bloc.dart';

class PasswordField extends StatelessWidget {
  const PasswordField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpBloc, SignUpState>(
      builder: (context, state) {
        return TextField(
          obscureText: true,
          onChanged: (value) {
            context.read<SignUpBloc>().add(SignUpPasswordChanedEvent(value));
          },
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: const OutlineInputBorder(),
            labelText: 'Password',
            errorText: state.password.invalid ? 'Invalid Password' : null,
          ),
        );
      },
    );
  }
}
