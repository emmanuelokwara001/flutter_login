import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

import '../../bloc/sign_up_bloc.dart';

class PhoneNumberField extends StatelessWidget {
  const PhoneNumberField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignUpBloc, SignUpState>(
      builder: (context, state) {
        return InternationalPhoneNumberInput(
          onInputChanged: (value) {
            context
                .read<SignUpBloc>()
                .add(SignUpPhoneNumberChanedEvent(value.phoneNumber!));
          },
          inputDecoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: const OutlineInputBorder(),
            labelText: 'Phone Number',
            errorText: state.number.invalid ? 'Invalid Phone Number' : null,
          ),
        );
      },
    );
  }
}
