import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/sign_up_bloc.dart';

class Surname extends StatelessWidget {
  const Surname({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: (value) {
        context.read<SignUpBloc>().add(SignUpSurnameChanedEvent(value));
      },
      decoration: const InputDecoration(
        contentPadding: EdgeInsets.all(10),
        border: OutlineInputBorder(),
        labelText: 'Surname',
      ),
    );
  }
}
