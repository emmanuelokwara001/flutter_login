import 'package:flutter/material.dart';
import 'sign_up_form.dart';

class Register extends StatelessWidget {
  const Register({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: SignUpForm(
            key: ValueKey('SignUpFormKey'),
          ),
        ),
      ),
    );
  }
}
