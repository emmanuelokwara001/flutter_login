import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:service_locator/service_locator.dart';
import 'package:user_repository/user_repository.dart';
import 'package:uuid/uuid.dart';

import '../../login/models/models.dart';
import '../../login/models/number.dart';

part 'sign_up_event.dart';
part 'sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final UserRepository _userRepo;
  final Uuid uuid = sl<Uuid>();
  SignUpBloc({userRepo})
      : _userRepo = userRepo,
        super(const SignUpState()) {
    on<SignUpNameChanedEvent>(_nameChaned);
    on<SignUpSurnameChanedEvent>(_surnameChanged);
    on<SignUpEmailChanedEvent>(_emailChanged);
    on<SignUpPhoneNumberChanedEvent>(_numberChanged);
    on<SignUpPasswordChanedEvent>(_onPasswordChanged);
    on<SignUpSubmitUserEvent>(_onSubmit);
    on<SignUpUsernameEvent>(_onUserNameChanged);
  }

  void _onUserNameChanged(
    SignUpUsernameEvent event,
    Emitter<SignUpState> emit,
  ) {
    Username username = Username.dirty(event.username);
    emit(
      state.copyWith(
        username: username,
        status: Formz.validate(
          [username],
        ),
      ),
    );
  }

  void _nameChaned(
    SignUpNameChanedEvent event,
    Emitter<SignUpState> emit,
  ) {
    Username name = Username.dirty(event.name);
    emit(
      state.copyWith(
        name: name,
        status: Formz.validate(
          [
            name,
          ],
        ),
      ),
    );
  }

  void _surnameChanged(
    SignUpSurnameChanedEvent event,
    Emitter<SignUpState> emit,
  ) {
    Username surname = Username.dirty(event.surname);
    emit(
      state.copyWith(
        surname: surname,
        status: Formz.validate(
          [
            surname,
          ],
        ),
      ),
    );
  }

  void _emailChanged(
    SignUpEmailChanedEvent event,
    Emitter<SignUpState> emit,
  ) {
    Email email = Email.dirty(event.email);
    emit(
      state.copyWith(
        email: email,
        status: Formz.validate(
          [
            email,
          ],
        ),
      ),
    );
  }

  void _numberChanged(
    SignUpPhoneNumberChanedEvent event,
    Emitter<SignUpState> emit,
  ) {
    PhoneNumber number = PhoneNumber.dirty(event.phoneNumber);
    emit(
      state.copyWith(
        number: number,
        status: Formz.validate(
          [
            number,
          ],
        ),
      ),
    );
  }

  void _onPasswordChanged(
      SignUpPasswordChanedEvent event, Emitter<SignUpState> emit) {
    Password pass = Password.dirty(event.password);
    emit(
      state.copyWith(
        password: pass,
        status: Formz.validate(
          [
            pass,
          ],
        ),
      ),
    );
  }

  void _onSubmit(
    SignUpSubmitUserEvent event,
    Emitter<SignUpState> emit,
  ) async {
    User user = User(
      id: uuid.v1(),
      surname: state.surname.value,
      username: state.username.value,
      name: state.name.value,
      password: state.password.value,
      phoneNumber: state.number.value,
    );

    bool userexists = await _userRepo.CheckUser(state.username.value);
    if (state.status.isValidated && !userexists) {
      _userRepo.addUser(user);
      return emit(
        state.copyWith(
          status: FormzStatus.submissionSuccess,
        ),
      );
    }
    return emit(
      state.copyWith(
        status: FormzStatus.submissionFailure,
      ),
    );
  }
}
