part of 'sign_up_bloc.dart';

class SignUpState extends Equatable {
  const SignUpState({
    this.name = const Username.pure(),
    this.surname = const Username.pure(),
    this.username = const Username.pure(),
    this.email = const Email.pure(),
    this.number = const PhoneNumber.pure(),
    this.password = const Password.pure(),
    this.status = FormzStatus.pure,
  });

  final Username name;
  final Username username;
  final Username surname;
  final Email email;
  final PhoneNumber number;
  final Password password;
  final FormzStatus status;

  @override
  List<Object> get props =>
      [name, surname, email, password, number, status, username];

  copyWith({
    Username? name,
    Username? surname,
    Username? username,
    Email? email,
    Password? password,
    PhoneNumber? number,
    FormzStatus? status,
  }) {
    return SignUpState(
      name: name ?? this.name,
      username: username ?? this.username,
      email: email ?? this.email,
      status: status ?? this.status,
      number: number ?? this.number,
      surname: surname ?? this.surname,
      password: password ?? this.password,
    );
  }
}
