part of 'sign_up_bloc.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();

  @override
  List<Object> get props => [];
}

class SignUpNameChanedEvent extends SignUpEvent {
  final String name;
  const SignUpNameChanedEvent(this.name);
}

class SignUpPasswordChanedEvent extends SignUpEvent {
  final String password;
  const SignUpPasswordChanedEvent(this.password);
}

class SignUpSurnameChanedEvent extends SignUpEvent {
  final String surname;
  const SignUpSurnameChanedEvent(this.surname);
}

class SignUpEmailChanedEvent extends SignUpEvent {
  final String email;
  const SignUpEmailChanedEvent(this.email);
}

class SignUpPhoneNumberChanedEvent extends SignUpEvent {
  final String phoneNumber;
  const SignUpPhoneNumberChanedEvent(this.phoneNumber);
}

class SignUpSubmitUserEvent extends SignUpEvent {}

class SignUpUsernameEvent extends SignUpEvent {
  final String username;
  const SignUpUsernameEvent(this.username);
}
