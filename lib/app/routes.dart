import 'package:go_router/go_router.dart';
import 'package:login_registration_app/ui/registration/ui/register.dart';
import 'package:login_registration_app/ui/registration/ui/sign_up_failed.dart';
import 'package:login_registration_app/ui/registration/ui/sign_up_success.dart';

import '../ui/home/view/home_page.dart';
import '../ui/login/view/login_page.dart';
import '../ui/splash/view/splash_page.dart';

// Routers class
class Routers {
  // final _navigatorKey = GlobalKey<NavigatorState>();
  static final router = GoRouter(
    initialLocation: '/splash',
    routes: <RouteBase>[
      GoRoute(
        path: '/splash',
        builder: (context, state) => const SplashPage(),
      ),
      GoRoute(
        path: '/login',
        builder: (context, state) => const LoginPage(),
      ),
      GoRoute(
        path: '/',
        builder: (context, state) => const HomePage(),
      ),
      GoRoute(
        path: '/signup',
        builder: (context, state) => const Register(),
        routes: [
          GoRoute(
            path: 'success',
            builder: (context, state) => const SignUpSuccessPage(),
          ),
          GoRoute(
            path: 'failure',
            builder: (context, state) => const SignUpErrorPage(),
          )
        ],
      )
    ],
  );
}
